package adamantClient;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import adamantShared.AttackMessage;
import adamantShared.Card;
import adamantShared.EndTurnMessage;
import adamantShared.GameOverMessage;
import adamantShared.Message;
import adamantShared.PickCardMessage;
import adamantShared.PlayCardMessage;
import adamantShared.RulesMessage;
import adamantShared.UpdateMessage;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


/**
 * This class controls the Battle session and is inherited from the Controller class.
 * This class records user interactions with the application.
 * It implements the Initializable interface and uses JavaFX worker class.
 * It is linked to an FXML file.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 *
 * @see adamantClient.ConnectionManager;
 * @see javafx.fxml.Initializable;
 * @see javafx.concurrent.WorkerStateEvent;
 * @see javafx.event.Event;
 * @see javafx.event.EventHandler;
 * @see javafx.scene.Node;
 * @see javafx.fxml.FXML;
 */

public class BattleController extends Controller 
{
    private boolean _active, _isAttackSourceSet, _isAttackDestinationSet;
    private int _cardWidth, _attackSource, _attackDestination;
    private ImageView _enemyHandImages[], _handImages[], _boardImages[], _enemyBoardImages[], _hoverImage;
    private HashMap<String, Image> _imageMap;
    private Tooltip _deckTooltip;
    private Media _cardSound, _fightSound, _nextTurnSound;
    
    @FXML
    private HBox _hoverBox;
    @FXML
    private ImageView _enemyDeckImage;
    @FXML
    private ImageView _deckImage;
    @FXML 
    private ImageView _actionPointsImage;
	@FXML
	private GridPane _battleground;
    @FXML
    private HBox _boardBox;
    @FXML
    private HBox _enemyBoardBox;
    @FXML
    private HBox _handBox;
    @FXML
    private HBox _enemyHandBox;
    @FXML
    private Text _nicknameLabel;
    @FXML
    private Text _enemyNicknameLabel;
    @FXML
    private Text _informationLabel;
    @FXML
    private Text _actionPointsText;
    @FXML
    private GridPane _left;
    @FXML 
    private AnchorPane _anchorPane;
    @FXML
    private Button _endTurnButton;
    @FXML
    private ImageView _hourGlass;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1)
    {    	
    	Font _cloisterBlackFont25px = Font.loadFont(new File("resources/assets/fonts/CloisterBlack.ttf").toURI().toString(), 25);
    	Font _cloisterBlackFont50px = Font.loadFont(new File("resources/assets/fonts/CloisterBlack.ttf").toURI().toString(), 50);
    	_anchorPane.getStylesheets().add(new File("resources/assets/css/style.css").toURI().toString());
    	_endTurnButton.getStylesheets().add(new File("resources/assets/css/style.css").toURI().toString());
    	_imageMap = new HashMap<String, Image>();
    	_deckTooltip = new Tooltip();
    	_isAttackSourceSet = false;
    	_isAttackDestinationSet = false;
    	_attackSource = -1;
    	_attackDestination = -1;
    	_hoverImage = new ImageView();
    	_hoverImage.setFitWidth(260);
		_hoverImage.setPreserveRatio(true);
		_hoverImage.setSmooth(true);
		_actionPointsImage.setImage(new Image(new File("resources/assets/logos/actionPoints.png").toURI().toString()));
    	_endTurnButton.setDisable(true);
    	_informationLabel.setFont(_cloisterBlackFont25px);
    	_actionPointsText.setFont(_cloisterBlackFont50px);
    	_informationLabel.setFill(Color.rgb(231, 231, 232));
    	_actionPointsText.setFill(Color.rgb(231, 231, 232));
    	_hourGlass.setImage(new Image(new File("resources/assets/logos/hourGlass.png").toURI().toString()));
    	
    	if(ClientInformation.getInstance().getPlaySounds())
    	{
    		_cardSound = new Media(new File("resources/assets/sounds/card.mp3").toURI().toString());
    		_fightSound = new Media(new File("resources/assets/sounds/fight.mp3").toURI().toString());
    		_nextTurnSound = new Media(new File("resources/assets/sounds/nextTurn.mp3").toURI().toString());
    	}
    	
    	for (int i = 0; i <= 5; ++i)
    	{
    		if(i == 0)
    		{
    			_imageMap.put("A" + i, new Image(new File("resources/assets/cards/attack/" + i + ".png").toURI().toString()));
    			_imageMap.put("C" + i, new Image(new File("resources/assets/cards/cost/" + i + ".png").toURI().toString()));
    			++i;
    		}

			_imageMap.put("C" + i, new Image(new File("resources/assets/cards/cost/" + i + ".png").toURI().toString()));
			_imageMap.put("A" + i, new Image(new File("resources/assets/cards/attack/" + i + ".png").toURI().toString()));
    		_imageMap.put("H" + i, new Image(new File("resources/assets/cards/health/" + i + ".png").toURI().toString()));
    		_imageMap.put("R" + i, new Image(new File("resources/assets/cards/health/red/" + i + ".png").toURI().toString()));
    	}
    	
    	for (int i = 6; i <= 7; ++i)
    	{
    		_imageMap.put("H" + i, new Image(new File("resources/assets/cards/health/" + i + ".png").toURI().toString()));
			_imageMap.put("A" + i, new Image(new File("resources/assets/cards/attack/" + i + ".png").toURI().toString()));
			_imageMap.put("R" + i, new Image(new File("resources/assets/cards/health/red/" + i + ".png").toURI().toString()));
    	}
    	
    	_messageReceiverService = new MessageReceiverService();
    	_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> {onRulesMessageReceived();});
    	_messageReceiverService.setOnFailed((WorkerStateEvent event) -> {ClientManager.exit();});
    	_messageReceiverService.start();
	}
    
    private void onRulesMessageReceived() 
    {
    	RulesMessage rules = (RulesMessage)_messageReceiverService.getValue();
    	_handImages = new ImageView[rules.getMaximumNumberOfCardsInHand()];
    	_enemyHandImages = new ImageView[rules.getMaximumNumberOfCardsInHand()];
    	_boardImages = new ImageView[rules.getMaximumNumberOfCardsOnBoard()];
    	_enemyBoardImages = new ImageView[rules.getMaximumNumberOfCardsOnBoard()];
    	
    	_cardWidth = (int)(_handBox.getWidth() - (rules.getMaximumNumberOfCardsInHand() - 1) * _handBox.getSpacing()) / rules.getMaximumNumberOfCardsInHand();
    	for(int i = 0; i < rules.getNumberOfCards(); ++i)
    		_imageMap.put("c" + i + ".png", new Image(new File("resources/assets/cards/" + i + ".png").toURI().toString()));
    	
    	for(int i = 0; i < rules.getNumberOfDecks(); ++i)
    		_imageMap.put("d" + i + ".png", new Image(new File("resources/assets/decks/" + i + ".png").toURI().toString()));
    	
    	for(int i = 0; i < rules.getMaximumNumberOfCardsInHand(); ++i)
    	{
    		_handImages[i] = new ImageView();
    		_enemyHandImages[i] = new ImageView();
			
			final int j = i;
			_handImages[i].addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
				
				playCard(j);
				event.consume();
			});
    	}
    	
    	for(int i = 0; i < rules.getMaximumNumberOfCardsOnBoard(); ++i)
    	{
    		_boardImages[i] = new ImageView();
    		_enemyBoardImages[i] = new ImageView();
    		
    		final int j = i;
    		_boardImages[i].addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
				
				_attackSource = j;
				_isAttackSourceSet = true;
				_isAttackDestinationSet = false;
				dropShadowSource();
				dropShadowDestination();
				event.consume();
			});
    		
    		_enemyBoardImages[i].addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
				
    			if(_isAttackSourceSet && _active)
    				attack();
    			
				event.consume();
			});
    	}
    	
    	_deckImage.setImage(_imageMap.get("d" + ClientInformation.getInstance().getDeckChoice() + ".png"));
		_deckImage.setFitWidth(150);
		_enemyDeckImage.setImage(_imageMap.get("d" + ClientInformation.getInstance().getEnemyDeckChoice() + ".png"));
		_enemyDeckImage.setFitWidth(150);
		_enemyDeckImage.setRotate(180);
    	
    	_deckImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
			
			pickCard();
			event.consume();
		});
		
    	_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> {onUpdateMessageReceived();});
		_messageReceiverService.restart();
	}

	private void onUpdateMessageReceived()
    {		
		_isAttackSourceSet = false;
		_isAttackDestinationSet = false;
		dropShadowSource();
		dropShadowDestination();
		Message receivedMessage = _messageReceiverService.getValue();
		
		if(receivedMessage instanceof GameOverMessage)
		{
			int winnerId = ((GameOverMessage)receivedMessage).getWinnerId();
			
			if(ClientInformation.getInstance().getId() == winnerId)
				_informationLabel.setText("You won the game !");

			else
				_informationLabel.setText("You lost the game...");
			
			ClientManager.exitWithAlert();
		}
		
		if(receivedMessage instanceof UpdateMessage)
		{
			UpdateMessage message = (UpdateMessage)receivedMessage;
		
			boolean wasActive = _active;
	    	_active = message.isActive();
	    	String information = new String();
	    	
	    	if(ClientInformation.getInstance().getPlaySounds() && (message.getAction().equals("played a card.") || message.getAction().equals("picked a card.")))
	    	{
	    		MediaPlayer mediaPlayer = new MediaPlayer(_cardSound);
    			mediaPlayer.setVolume(0.5);
    			mediaPlayer.play();
    		}
	    	
	    	else if(ClientInformation.getInstance().getPlaySounds() && message.getAction().equals("attacked a card."))
	    	{
	    		MediaPlayer mediaPlayer = new MediaPlayer(_fightSound);
    			mediaPlayer.setVolume(0.5);
    			mediaPlayer.play();
    		}
	    	
	    	if(_active)
			{
	    		if(!wasActive && ClientInformation.getInstance().getPlaySounds())
	    		{
		    		MediaPlayer mediaPlayer = new MediaPlayer(_nextTurnSound);
	    			mediaPlayer.setVolume(0.5);
	    			mediaPlayer.play();
	    		}
	    		
	    		if(!message.getAction().isEmpty())
	    			information = "\nYou " + message.getAction();
	    			
				_informationLabel.setText(ClientInformation.getInstance().getName() + ",\nit is your turn." + information);
				_endTurnButton.setDisable(false);
				_actionPointsText.setText(Integer.toString(message.getActionPoints()));
				_hourGlass.setVisible(false);
			}
	    	
	    	else
			{
	    		if(!message.getAction().isEmpty())
	    			information = "\nYour opponent " + message.getAction();
	    		
	    		_informationLabel.setText(ClientInformation.getInstance().getName() +",\nwait for your turn." + information);
				_endTurnButton.setDisable(true);
				_actionPointsText.setText(Integer.toString(message.getEnemyActionPoints()));
				_hourGlass.setVisible(true);
			}

	    	_deckTooltip.setText(message.getDeckSize() + " cards left in your deck");
	    	Tooltip.install(_deckImage, _deckTooltip);    	
	    	
	    	_enemyHandBox.getChildren().clear();
	    	_handBox.getChildren().clear();
	    	_boardBox.getChildren().clear();
	    	_enemyBoardBox.getChildren().clear();
	    	
	    	for(int i = 0; i < message.getEnemyHandSize(); ++i)
	    	{
	    		_enemyHandImages[i].setFitWidth(_cardWidth);
	    		_enemyHandImages[i].setPreserveRatio(true);
	    		_enemyHandImages[i].setSmooth(true);
	    		_enemyHandImages[i].setImage(_imageMap.get("d" + ClientInformation.getInstance().getEnemyDeckChoice() + ".png"));
	    		_enemyHandImages[i].setRotate(180);
	    		_enemyHandBox.getChildren().add(_enemyHandImages[i]);
	    	}
	    	
	    	for(int i = 0; i < message.getHand().size(); ++i)
	    	{    		
	    		_handImages[i].setFitWidth(_cardWidth);
	    		_handImages[i].setPreserveRatio(true);
	    		_handImages[i].setSmooth(true);
	    		_handImages[i].setImage(_imageMap.get("c" + message.getHand().get(i).getFileName()));
				_handBox.getChildren().add(createGroup(_handImages[i], message.getHand().get(i), _cardWidth));
				
				final int j = i;
				_handImages[i].addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
					
					_hoverBox.getChildren().clear();
					_hoverImage.setImage(_handImages[j].getImage());
					_hoverBox.getChildren().add(createGroup(_hoverImage, message.getHand().get(j), 260));
					event.consume();
				});
				
				_handImages[i].addEventHandler(MouseEvent.MOUSE_EXITED, event -> {
					
					_hoverImage.setImage(null);
					_hoverBox.getChildren().clear();
					event.consume();
				});
	    	}
	    	
	    	for(int i = 0; i < message.getBoard().size(); ++i)
	    	{
	    		_boardImages[i].setFitWidth(_cardWidth);
	    		_boardImages[i].setPreserveRatio(true);
	    		_boardImages[i].setSmooth(true);
	    		_boardImages[i].setImage(_imageMap.get("c" + message.getBoard().get(i).getFileName()));		
				_boardBox.getChildren().add(createGroup(_boardImages[i], message.getBoard().get(i), _cardWidth));
				
				final int j = i;
				_boardImages[i].addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
					
					_hoverBox.getChildren().clear();
					_hoverImage.setImage(_boardImages[j].getImage());
					_hoverBox.getChildren().add(createGroup(_hoverImage, message.getBoard().get(j), 260));
					event.consume();
				});
				
	    		_boardImages[i].addEventHandler(MouseEvent.MOUSE_EXITED, event -> {
					
					_hoverImage.setImage(null);
					_hoverBox.getChildren().clear();
					event.consume();
				});
	    	}
	    	
	    	for(int i = 0; i < message.getEnemyBoard().size(); ++i)
	    	{
	    		prepareImageView(_enemyBoardImages[i], _cardWidth);
	    		_enemyBoardImages[i].setImage(_imageMap.get("c" + message.getEnemyBoard().get(i).getFileName()));
	    		_enemyBoardBox.getChildren().add(_enemyBoardImages[i]);
	    		_enemyBoardBox.getChildren().add(createGroup(_enemyBoardImages[i], message.getEnemyBoard().get(i), _cardWidth));
	    		
	    		final int j = i;
	    		_enemyBoardImages[i].addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
	    			
	    			_hoverBox.getChildren().clear();
					_hoverImage.setImage(_enemyBoardImages[j].getImage());
					_hoverBox.getChildren().add(createGroup(_hoverImage, message.getEnemyBoard().get(j), 260));
					
					if(_isAttackSourceSet && _active)
	    			{
	    				_isAttackDestinationSet = true;
	    				_attackDestination = j;
	    				dropShadowDestination();
	    			}
					
					event.consume();
				});
				
	    		_enemyBoardImages[i].addEventHandler(MouseEvent.MOUSE_EXITED, event -> {
					
					_hoverImage.setImage(null);
					_hoverBox.getChildren().clear();
					
					if(_isAttackSourceSet && _active)
	    			{
	    				_isAttackDestinationSet = false;
	    				dropShadowDestination();
	    			}
					
					event.consume();
				});
	    	}
	    	
	    	if(!_active)
	    	{
	    		_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> {onUpdateMessageReceived();});
	    		_messageReceiverService.restart();
	    	}
		}
		
		else
			ClientManager.exit();
    }
    
    private void pickCard() 
    {
    	if(_active)
    	{
    		ConnectionManager.getInstance().send(new PickCardMessage());
    		_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> {onUpdateMessageReceived();});
    		_messageReceiverService.restart();
    	}
	}
    
    private void playCard(int cardPosition) 
    {
    	if(_active)
    	{
    		ConnectionManager.getInstance().send(new PlayCardMessage(cardPosition));
    		_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> {onUpdateMessageReceived();});
    		_messageReceiverService.restart();
    	}	
	}
    
    private void attack()
    {
    	ConnectionManager.getInstance().send(new AttackMessage(_attackSource, _attackDestination));
    	_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> {onUpdateMessageReceived();});
    	_messageReceiverService.restart();	
    }

	@FXML
    private void endTurn()
    {
    	if(_active)
    	{
    		ConnectionManager.getInstance().send(new EndTurnMessage());
    		_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> {onUpdateMessageReceived();});
    		_messageReceiverService.restart();
    	}
    }
	
	private void dropShadowSource()
	{
		for(int i = 0; i < _boardImages.length; ++i)
			_boardImages[i].setEffect(new DropShadow(0.0, 0.0, 0.0, Color.GRAY));
		if(_isAttackSourceSet)
			_boardImages[_attackSource].setEffect(new DropShadow(40.0, 0.0, 0.0, Color.GRAY));
	}
	
	private void dropShadowDestination()
	{
		
		for(int i = 0; i < _enemyBoardImages.length; ++i)
			_enemyBoardImages[i].setEffect(new DropShadow(0.0, 0.0, 0.0, Color.GRAY));
		
		if(_isAttackDestinationSet)
			_enemyBoardImages[_attackDestination].setEffect(new DropShadow(40.0, 0.0, 0.0, Color.GRAY));
	}
	
	private Group createGroup(ImageView imageView, Card card, int width)
	{
		ImageView attackImage = new ImageView();
		ImageView costImage = new ImageView();
		ImageView healthImage = new ImageView();
		
		attackImage.setFitWidth(width);
		attackImage.setPreserveRatio(true);
		attackImage.setSmooth(true);
		attackImage.setImage(_imageMap.get("A" + card.getAttack()));
		attackImage.setBlendMode(BlendMode.SRC_OVER);
		
		costImage.setFitWidth(width);
		costImage.setPreserveRatio(true);
		costImage.setSmooth(true);
		costImage.setImage(_imageMap.get("C" + card.getCost()));
		costImage.setBlendMode(BlendMode.SRC_OVER);
		
		healthImage.setFitWidth(width);
		healthImage.setPreserveRatio(true);
		healthImage.setSmooth(true);
		
		if(card.getCurrentHealth() != card.getHealth())
			healthImage.setImage(_imageMap.get("R" + card.getCurrentHealth()));
		else
			healthImage.setImage(_imageMap.get("H" + card.getCurrentHealth()));
		
		healthImage.setBlendMode(BlendMode.SRC_OVER);
		
		return new Group(imageView, attackImage, costImage, healthImage);
	}
	
	void prepareImageView(ImageView imageView, double width)
	{
		imageView.setFitWidth(width);
		imageView.setPreserveRatio(true);
		imageView.setSmooth(true);
	}
}

