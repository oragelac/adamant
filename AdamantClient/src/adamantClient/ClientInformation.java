package adamantClient;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * This class maintains client information through the entire application life time. 
 * This information may be accessed and modified using getter and setter methods.
 * This class is a singleton.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public class ClientInformation 
{
	private int _id, _deckChoice, _enemyDeckChoice;
	private String _name, _enemyName;
	private boolean _playMusic, _playSounds;
	
	// SINGLETON IMPLEMENTATION
	static private ClientInformation clientInformation = null;
	
	/**
     * Class constructor.
     * It is declared as private to prevent its creation from outer classes.
    */
	
	private ClientInformation()
	{
		readInformations();
	}
	
	/**
     * This method returns a pointer to the ClientInformation object. 
     * If the object does not exist yet, it creates it.
     * @return	ClientInformation
    */
	static public ClientInformation getInstance()
	{
		if(clientInformation == null)
		{
			clientInformation = new ClientInformation();
		}

		return clientInformation;
	}
	
	public void readInformations()
	{
		try 
		{
			Stream<String> lines = Files.lines(Paths.get("resources/config/client.config"));
			lines.forEach(s -> this.parse(s));
			lines.close();
		} 
		
		catch (IOException e) 
		{
			System.out.println("-- ERROR -- client.config not found");
		}
	}

	/**
	 * This method separates the config file into lines then read the information.
	 * This method uses the class setters.
	 */

	private void parse(String s) 
	{
		String[] splittedLine = s.split(" ");
		if(splittedLine.length == 2)
		{
			if(splittedLine[0].equals("sounds"))
			{
				_playSounds = false;
				if(splittedLine[1].equals("on"))
					_playSounds = true;
			}
			
			else if(splittedLine[0].equals("music"))
			{
				_playMusic = false;
				if(splittedLine[1].equals("on"))
					_playMusic = true;
			}
			
			else
			{
				System.out.println("Invalid parameter");
			}
		}
		
		else
		{
			System.out.println("Invalid configuration file structure");
		}
	}
	
	public boolean getPlaySounds()
	{
		return _playSounds;
	}
	
	public boolean getPlayMusic()
	{
		return _playMusic;
	}
	
	/**
     * This is the id getter method. 
     * 
     * @return	client id.
    */
	
	public int getId()
	{
		return _id;
	}
	
	/**
     * This is the id setter method. 
     * 
     * @param	client id.
    */

	public void setId(int id)
	{
		_id = id;
	}
	
	/**
     * This is the name getter method. 
     * 
     * @return	client name.
    */

	public String getName()
	{
		return _name;
	}

	/**
     * This is the name setter method. 
     * 
     * @param	client name.
    */
	
	public void setName(String playerName)
	{
		_name = playerName;
	}	
	
	/**
     * This is the enemy name setter method. 
     * 
     * @return	enemy client name.
    */
	
	public String getEnemyName()
	{
		return _enemyName;
	}

	/**
     * This is the enemy name setter method. 
     * 
     * @param	enemy client name.
    */
	
	public void setEnemyName(String playerName)
	{
		_enemyName = playerName;
	}
	
	/**
     * This is the deck choice getter method. 
     * 
     * @return	client deck choice.
    */
	
	public int getDeckChoice()
	{
		return _deckChoice;
	}
	
	/**
     * This is the enemy deck choice getter method. 
     * 
     * @return	enemy client deck choice.
    */
	
	public int getEnemyDeckChoice()
	{
		return _enemyDeckChoice;
	}
	
	/**
     * This is the deck choice setter method. 
     * 
     * @param	client deck choice.
    */
	
	public void setDeckChoice(int _deckChoice)
	{
		this._deckChoice = _deckChoice;
	}
	
	/**
     * This is the enemy deck choice setter method. 
     * 
     * @param	enemy client deck choice.
    */
	
	public void setEnemyDeckChoice(int _enemyDeckChoice)
	{
		this._enemyDeckChoice = _enemyDeckChoice;
	}
}
