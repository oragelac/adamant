package adamantClient;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.IOException;

/**
 * This class is the entry point of the client application. 
 * It initializes JavaFX context and launch the graphical interface.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 * @see     javafx.application.Application
 * @see     javafx.scene.Scene
 * @see     javafx.stage.Stage
 * @see     javafx.fxml.FXMLLoader
*/

public class ClientManager extends Application
{
    // JavaFX objects
    private Stage window;
    private LoginController cLogin;
    private PreBattleController cPreBattle;
    private BattleController cBattle;

    /**
     * This is the main method. 
     * It calls the inherited method "launch(String...)" from the Application JavaFX class.
     *
     * @see     javafx.application.Application
     * @param	args 
    */

    public static void main(String[] args)
    {
        launch(args);
    }
    
    /**
     * This method is called by the method "launch(String...)". 
     * It initializes the main Stage and start the first controller.
     *
     * @see 	Controller
     * @see		javafx.stage.Stage
     * @param	javafx.stage.Stage
    */

    @Override
    public void start(Stage primaryStage) 
    {
        System.out.println("Client initializing...");
        System.out.println("");

        // Stage initialization
        window = primaryStage;
        primaryStage.setTitle("Adamant");

        primaryStage.initStyle(StageStyle.UNDECORATED);

        startController(cLogin, "Login.fxml");
    }
    
    /**
     * This method returns the pre-battle controller. 
     *
     * @see 	Controller
     * @see		PreBattleController
     * @return 	PreBattleController
    */

    public PreBattleController getcPreBattle() 
    {
        return cPreBattle;
    }
    
    /**
     * This method returns the battle controller. 
     *
     * @see 	Controller
     * @see		BattleController
     * @return 	BattleController
    */
    
    public BattleController getcBattle()
    {
        return cBattle;
    }

    /**
     * This method launches the controller passed as an argument. 
     *
     * @see 	Controller
     * @see		javafx.fxml.FXMLLoader
     * @param	Controller
     * @param	String
    */
    
    public void startController(Controller controller, String fxml)
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
        try
        {
            Scene scene = new Scene( (Parent) loader.load());
            
			window.setScene(scene);
		} 
        
        catch (IOException e) 
        {
			System.out.println("-- CM:ERROR -- Controller loading failed @" + fxml);
		}
        
        controller = loader.<Controller>getController();
        controller.setClientManager(this);
        
        window.centerOnScreen();
        window.show();
    }
    
    /**
     * This method shuts the application down. 
     * This method is static.
     *
     * @see 	javafx.application.Platform
    */
    
    public static void exit()
    {
        System.out.println("Client shutting down...");
        Platform.exit();
    }

    /**
     * This method displays an alert dialog box and shuts the application down. 
     * This method is static.
     *
     * @see 	javafx.application.Platform
    */
    
    public static void exitWithAlert()
    {
    	Alert alert = new Alert(AlertType.WARNING);
    	alert.setContentText("Opponent has quit the game or the connection with the server has been lost.");
    	alert.showAndWait().filter(response -> response == ButtonType.OK);
        System.out.println("Client shutting down...");
        Platform.exit();
    }
}