package adamantClient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import adamantShared.Message;

/**
 * This class handles all the network input and output access.
 * It uses the ServerInformation class to store server-related information.
 * This class is a singleton.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 * @see adamantClient.ServerInformation
 */

public class ConnectionManager 
{
	static private ConnectionManager connectionManager = null;

	/**
	 * Class constructor.
	 * It is declared as private to prevent its creation from outer classes.
	 */

	private ConnectionManager()
	{
		_serverInformation = new ServerInformation();
		_connected = false;
	}

	/**
	 * This method returns a pointer to the ConnectionManager object.
	 * If the object does not exist yet, it creates it.
	 * @return	ConnectionManager
	 */

	static public ConnectionManager getInstance()
	{
		if(connectionManager == null)
		{
			connectionManager = new ConnectionManager();
		}

		return connectionManager;
	}

	private ServerInformation _serverInformation;
	private Socket _socket;
	private ObjectOutputStream _outToServer;
	private ObjectInputStream _inFromServer;
	private boolean _connected;

	/**
	 * This method initialize the TCP interface.
	 * This method uses the server information stored in the ServerInformation attribute.
	 * This method uses the Java builtin network interface.
	 * It returns true if the connection is successful and false if the server is unreachable.
	 *
	 * @see adamantClient.ServerInformation
	 * @see java.io.ObjectInputStream
	 * @see java.io.ObjectOutputStream
	 * @see java.net.InetSocketAddress
	 * @see java.net.Socket
	 *
	 * @return the state of the connection
	 */

    public boolean connect()
    {
        try 
        {
            System.out.println("TCP interface initialization...");
            _socket = new Socket();
            _socket.connect(new InetSocketAddress(_serverInformation.getHostName(), _serverInformation.getPort()), 1000);
            _outToServer = new ObjectOutputStream(_socket.getOutputStream());
            _inFromServer = new ObjectInputStream(_socket.getInputStream());
            _connected = true;
			return true;
        } 
        
        catch (IOException e)
        {
            System.out.println("-- CM:ERROR 0 -- Server not found");
            return false;
        }
    }

	/**
	 * This method closes all the network-related attributes.
	 * It returns true if the disconnection is successful and false if the connection didn't exist.
	 *
	 * @see java.io.ObjectInputStream
	 * @see java.io.ObjectOutputStream
	 * @see java.net.InetSocketAddress
	 * @see java.net.Socket
	 *
	 * @return the success of the disconnection
	 */

    public boolean disconnect()
    {
        try 
        {
            _outToServer.close();
            _inFromServer.close();
            _socket.close();
            _connected = false;
        } 
        
        catch (NullPointerException e)
        {
            System.out.println("-- CM:WARNING -- Connection doesn't exist");
            return false;
        } 
        
        catch (IOException e)
        {
            System.out.println("-- CM:WARNING -- Connection not initialized");
            return false;
        } 
        
        return true;
    }

	/**
	 * This method sends a message through the ObjectOutputStream attribute.
	 * It warns the user and leaves the application in case of failure.
	 *
	 * @see java.io.ObjectOutputStream;
	 */

	public void send(Message message)
	{
		try
		{
			_outToServer.writeObject(message);
		}
		
		catch (IOException e)
		{
			ClientManager.exitWithAlert();
		}
	}

	/**
	 * This method waits for a message reception from the server through the ObjectInputStream attribute.
	 * It warns the user and leaves the application in case of failure.
	 *
	 * @see adamantShared.Message;
	 * @see java.io.ObjectOutputStream;
	 *
	 * @return the message
	 */

	public Message receive() throws IOException
	{
		Message msg = new Message();
		
		try
		{
			msg = (Message) _inFromServer.readObject();
		} 
		
		catch (ClassNotFoundException e)
		{
			System.out.println("-- CM:ERROR 1 -- Server message misunderstood");
			ClientManager.exitWithAlert();
		} 
		
		return msg;
	}

	/**
	 * This is the _connected getter method.
	 *
	 * @return the connection state
	 */

	public boolean isConnected()
	{
		return _connected;
	}
}
