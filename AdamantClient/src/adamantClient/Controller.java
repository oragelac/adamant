package adamantClient;

import adamantShared.ExitMessage;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * This class is abstract thus used only to inherit other classes.
 * Inherited children are intended to be used in conjunction with FXML files.
 * This class implements the Initializable interface;
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 *
 * @see javafx.fxml.FXML;
 * @see adamantShared.ExitMessage;
 * @see javafx.fxml.Initializable;
 */

abstract public class Controller implements Initializable
{
	protected MessageReceiverService _messageReceiverService;
    public ClientManager _clientManager;

    /**
     * This method sets a pointer to the ClientManager;
     *
     * @see adamantClient.ClientManager;
     * @param ClientManager;
     */

    public void setClientManager(ClientManager clientManager)
    {
        _clientManager = clientManager;
    }

    /**
     * This method closes the network connection if it exists and exits the application ;
     *
     * @see adamantClient.ConnectionManager;
     * @see adamantClient.ClientManager;
     */

    @FXML
    protected void exit()
    {
    	if(ConnectionManager.getInstance().isConnected())
    	{
    		ConnectionManager.getInstance().send(new ExitMessage());
    	}
    	
    	ClientManager.exit();
    }
}
