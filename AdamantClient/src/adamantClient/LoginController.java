package adamantClient;

import adamantShared.SalutationMessage;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class controls the login session and is inherited from the Controller class.
 * This class setups the first contacts with the server through the use of the ConnectionManager class.
 * It implements the Initializable interface and uses JavaFX worker class.
 * It is linked to an FXML file.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 *
 * @see adamantClient.ConnectionManager
 * @see javafx.fxml.Initializable
 * @see javafx.concurrent.WorkerStateEvent
 * @see javafx.fxml.FXML
 * @see javafx.scene.Node
 */

public class LoginController extends Controller
{
    // FXML-Accessible variables
    @FXML
    private TextField _nicknameTextField;
    @FXML
    private Text _informationText;
    @FXML
    private Button _connectButton;
    @FXML
    private Button _exitButton;
    @FXML
    private ImageView _logo;
    @FXML
    private GridPane _gridPane;

    /**
     * This method is inherited from the Intializable interface.
     * It setups basic gui information.
     *
     * @see javafx.fxml.Initializable
     * @see javafx.scene.Node
     * @param arg0
     * @param arg1
     */

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) 
    {
    	_informationText.setText("Choose a nickname");
        _informationText.setFill(Color.BLACK);
        _gridPane.getStylesheets().add(new File("resources/assets/css/style.css").toURI().toString());
        _logo.setImage(new Image(new File("resources/assets/logos/logoBlack.png").toURI().toString()));
    
        if(ClientInformation.getInstance().getPlayMusic())
        {
        	MediaPlayer mediaPlayer = new MediaPlayer(new Media(new File("resources/assets/sounds/background.mp3").toURI().toString()));
        	mediaPlayer.setAutoPlay(true);
        	mediaPlayer.setCycleCount(Integer.MAX_VALUE);
        }
    }

    /**
     * This method uses JavaFX worker to dynamically update the GUI depending on the network connection status.
     *
     * @see javafx.fxml.FXML
     * @see	javafx.concurrent.WorkerStateEvent
     * @see adamantShared.Message
     */

    public void onMessageReceived()
    {
    	SalutationMessage message = (SalutationMessage)_messageReceiverService.getValue();
    	
    	if (message.getAction().equals("Hi")) 
        {
        	ClientInformation.getInstance().setId(message.getId());
            System.out.println("-- LC:INFO -- Connection established.");
            _informationText.setText("Connection established !");
            _informationText.setFill(Color.GREEN);
            _messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> onSalutationMessageReceived());
            _messageReceiverService.restart();
        } 
        
        else 
        {
            System.out.println("-- LC:WARNING -- Server busy.");
            _informationText.setText("Server busy");
            _informationText.setFill(Color.RED);
        }
    }

    /**
     * This method stores the enemy nickname through the ClientManager pointer and launch the preBattle Controller.
     *
     * @see javafx.fxml.FXML;
     * @see	adamantClient.ClientManager
     * @see adamantClient.PreBattleController
     */

    private void onSalutationMessageReceived() 
    {
    	SalutationMessage message = (SalutationMessage)_messageReceiverService.getValue();
    	ClientInformation.getInstance().setEnemyName(message.getNickname());
    	_clientManager.startController(_clientManager.getcPreBattle(), "PreBattle.fxml");
	}

    /**
     * This method is the main method of the class.
     * It verifies then gets the user inputs then stores them through the use of the ClientManager.
     * This method calls the ConnectionManager methods to connect to the server.
     * This method is FXML-accessible and calls JavaFX workers to update GUI.
     *
     * @see adamantClient.ClientManager
     * @see adamantClient.ConnectionManager
     * @see javafx.concurrent.WorkerStateEvent
     * @see javafx.scene.Node
     */

    @FXML
    private void connect() 
    {
        String nickname = _nicknameTextField.getText();

        if (nickname.isEmpty())
        {
            System.out.println("-- LC:WARNING -- Nickname empty.");
            _informationText.setText("Pick a nickname");
            _informationText.setFill(Color.RED);
        } 
        
        else if (ConnectionManager.getInstance().connect())
        {
        	_connectButton.setDisable(true);
        	_exitButton.setDisable(true);
        	_nicknameTextField.setDisable(true);
        	_informationText.setText("Waiting for the opponent to connect...");
        	_informationText.setFill(Color.GREEN);
            ClientInformation.getInstance().setName(nickname);
            ConnectionManager.getInstance().send(new SalutationMessage(nickname));
            _messageReceiverService = new MessageReceiverService();
            _messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> onMessageReceived());
            _messageReceiverService.setOnFailed((WorkerStateEvent event) -> ClientManager.exit());
            _messageReceiverService.start();
        } 
        
        else 
        {
            System.out.println("-- LC:WARNING -- Server unreachable.");
            _informationText.setText("Server unreachable");
            _informationText.setFill(Color.RED);
        }
    }
}
