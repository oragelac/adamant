package adamantClient;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import adamantShared.Message;

/**
 * This class creates a task executed in a separate thread.
 * This tasks consists in a receive method call to the ConnectionManager.
 * This class extends the JavaFX concurrent Service class.
 *
 * @see javafx.concurrent.Service
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public class MessageReceiverService extends Service<Message>
{
	/**
     * This methods creates the task to be executed in a separated thread.
     * This is a override method.
    */
	
	@Override
	protected Task<Message> createTask() 
	{
		return new Task<Message>()
		{
			@Override
			protected Message call() throws Exception 
			{
				Message message = ConnectionManager.getInstance().receive();
				return message;
			}
		
		};
	}
}
