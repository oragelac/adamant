package adamantClient;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import adamantShared.DeckChoiceMessage;
import adamantShared.ReadyMessage;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * This class controls the preBattle session and is inherited from the Controller class.
 * This class records user interactions with the application.
 * It implements the Initializable interface and uses JavaFX worker class.
 * It is linked to an FXML file.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 *
 * @see adamantClient.ConnectionManager;
 * @see javafx.fxml.Initializable;
 * @see javafx.concurrent.WorkerStateEvent;
 * @see javafx.event.Event;
 * @see javafx.event.EventHandler;
 * @see javafx.scene.Node;
 * @see javafx.fxml.FXML;
 */

public class PreBattleController extends Controller
{
	private ArrayList<String> _names, _descriptions;
	private int _selectedDeck;
	private ImageView _deckImages[];
	private ImageView _deckLogos[];

	// FXML-Accessible variables
    @FXML
    private Text _nicknameText, _enemyNicknameText, _deckNameText, _deckDescriptionText, _informationText, _vsText;
    @FXML
    private Button _selectButton;
    @FXML
    private Button _readyButton;
	@FXML
	private VBox _deckLogo;
    @FXML
    private HBox _box;
    @FXML
    private ImageView _logo;
    @FXML
    private AnchorPane _anchorPane;

	/**
	 * This method is inherited from the Intializable interface.
	 * It setups basic GUI information.
	 *
	 * @see javafx.fxml.Initializable
	 * @see javafx.scene.Node
	 * @param arg0
	 * @param arg1
	 */

    @Override
	public void initialize(URL arg0, ResourceBundle arg1) 
    {
    	Font _cloisterBlackFont30px = Font.loadFont(new File("AdamantClient/resources/assets/fonts/CloisterBlack.ttf").toURI().toString(), 30);
    	Font _cloisterBlackFont50px = Font.loadFont(new File("AdamantClient/resources/assets/fonts/CloisterBlack.ttf").toURI().toString(), 50);
    	_anchorPane.getStylesheets().add(new File("AdamantClient/resources/assets/css/style.css").toURI().toString());
    	_selectedDeck = -1;
    	_readyButton.setVisible(false);
    	_selectButton.setDisable(true);
    	_informationText.setText("Which side will you join ?");
    	_nicknameText.setFont(_cloisterBlackFont30px);
    	_nicknameText.setText(ClientInformation.getInstance().getName());
		_nicknameText.setTextAlignment(TextAlignment.CENTER);
		_vsText.setText("VS");
    	_vsText.setFont(_cloisterBlackFont50px);
		_enemyNicknameText.setFont(_cloisterBlackFont30px);
    	_enemyNicknameText.setText(ClientInformation.getInstance().getEnemyName());
		_enemyNicknameText.setTextAlignment(TextAlignment.CENTER);
		_messageReceiverService = new MessageReceiverService();
		_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> onDeckChoiceMessageReceived());
		_messageReceiverService.setOnFailed((WorkerStateEvent event) -> ClientManager.exit());
		_messageReceiverService.start();	
		_logo.setImage(new Image(new File("AdamantClient/resources/assets/logos/logoBlack.png").toURI().toString()));
    }

	/**
	 * This method dynamically loads the Deck selection GUI through the use of the Message Receiver Service.
	 * This method handles the deck selection event in itself and by the use of the updateDeckInformation method.
	 *
	 * @see adamantClient.MessageReceiverService;
	 * @see adamantShared.DeckChoiceMessage;
	 * @see javafx.event.Event;
	 * @see javafx.event.EventHandler;
	 * @see javafx.scene.Node;
	 */

	private void onDeckChoiceMessageReceived()
    {
    	DeckChoiceMessage message = (DeckChoiceMessage)_messageReceiverService.getValue();

		// Get the deck information back from the message received
		_names = message.getNames();
		_descriptions = message.getDescriptions();
		int numberOfDecks = message.getNames().size();

		// Image loading system
		_deckImages = new ImageView[numberOfDecks];
		_deckLogos = new ImageView[numberOfDecks];
		for(int i = 0; i < numberOfDecks; ++i)
		{
			// Loading of the deck images
			_deckImages[i] = new ImageView();
			_deckImages[i].setImage(new Image(new File("AdamantClient/resources/assets/decks/" + i + ".png").toURI().toString()));
			_deckImages[i].setFitWidth(240);
			_deckImages[i].setPreserveRatio(true);
			_deckImages[i].setSmooth(true);
			// Loading of the deck logos
			_deckLogos[i] = new ImageView();
			_deckLogos[i].setImage(new Image(new File("AdamantClient/resources/assets/logos/" + i + ".png").toURI().toString()));
			_deckLogos[i].setFitWidth(100);
			_deckLogos[i].setPreserveRatio(true);
			_deckLogos[i].setSmooth(true);

			// Put it in the array
			_box.getChildren().add(_deckImages[i]);

			final int j = i;

			// Create the click handler per image
			_deckImages[i].addEventHandler(MouseEvent.MOUSE_CLICKED, event ->
			{
				_selectedDeck = j;
				updateDeckInformation(numberOfDecks);
				event.consume();
			});
		}
    }

	/**
	 * This method dynamically updates the Deck selection GUI and relative information.
	 * This method uses effects to warn the user of his choices.
	 *
	 * @see javafx.event.Event
	 * @see javafx.event.EventHandler
	 * @see javafx.scene.Node
	 * @see javafx.scene.effect.Effect
	 *
	 * @param numberOfDecks
	 */

    private void updateDeckInformation(int numberOfDecks)
    {
    	_selectButton.setDisable(false);
    	_informationText.setText("Click select to confirm your choice.");
    	
		// Remove the drop shadow from the other decks
		for(int i=0; i<numberOfDecks; ++i)
		{
			if(i!=_selectedDeck){_deckImages[i].setEffect(new DropShadow(0.0, 0.0, 0.0, Color.GRAY));}
		}
		// Put the drop shadow on the chosen deck
		_deckImages[_selectedDeck].setEffect(new DropShadow(80.0, 0.0, 0.0, Color.GRAY));

		// Update the information panel
		_deckLogo.getChildren().clear();
		_deckLogo.getChildren().add(_deckLogos[_selectedDeck]);
    	_deckNameText.setText(_names.get(_selectedDeck));
    	_deckDescriptionText.setText(_descriptions.get(_selectedDeck));
    	_deckDescriptionText.setWrappingWidth(200);
    	_deckDescriptionText.setTextAlignment(TextAlignment.CENTER);
	}

	/**
	 * This method is the main method of the class.
	 * It gets the user inputs and update the Client Information class
	 * and then warns the ConnectionManager of the user's choice..
	 * This method is FXML-accessible.
	 *
	 * @see adamantClient.ClientInformation
	 * @see adamantClient.ConnectionManager
	 * @see javafx.scene.Node
	 */

	@FXML
	private void select()
    {
		_informationText.setText("Click ready to start the battle.");
		
   		_selectButton.setDisable(true);
   		_readyButton.setVisible(true);
   		_box.setDisable(true);
    	System.out.println("-- INFO -- Selected deck : " + _selectedDeck);
    	ClientInformation.getInstance().setDeckChoice(_selectedDeck);
   		ConnectionManager.getInstance().send(new DeckChoiceMessage(_selectedDeck));
    }

	/**
	 * This method receives and react to the enemy deck choice through the use of the MessageReceiverService.
	 *
	 * @see adamantClient.MessageReceiverService
	 * @see javafx.concurrent.WorkerStateEvent
	 */

	private void onReadyMessageReceived() 
	{
		_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> onEnemyDeckChoiceMessageReceived());
   		_messageReceiverService.restart();
	}

	/**
	 * This method updates the GUI while the opponent chooses his deck.
	 *
	 * @see adamantClient.ConnectionManager
	 * @see adamantClient.MessageReceiverService
	 * @see javafx.concurrent.WorkerStateEvent
	 * @see javafx.scene.Node
	 */

	@FXML
	private void ready()
    {
		_informationText.setText("Waiting for the opponent to be ready...");
		_informationText.setFill(Color.GREEN);
		_readyButton.setDisable(true);
    	System.out.println("-- INFO -- Player is ready to start the battle.");
    	ConnectionManager.getInstance().send(new ReadyMessage());
    	_messageReceiverService.setOnSucceeded((WorkerStateEvent event) -> onReadyMessageReceived());
   		_messageReceiverService.restart();
    }

	/**
	 * This method stores the enemy deck choice in the Client Information.
	 * This method launches the Battle Controller.
	 *
	 * @see adamantShared.Message
	 * @see adamantClient.MessageReceiverService
	 * @see adamantClient.ClientInformation
	 * @see adamantClient.BattleController
	 */

	private void onEnemyDeckChoiceMessageReceived()
	{
		DeckChoiceMessage message = (DeckChoiceMessage)_messageReceiverService.getValue();
		ClientInformation.getInstance().setEnemyDeckChoice(message.getDeckId());
		_clientManager.startController(_clientManager.getcBattle(), "Battle.fxml");
	}
}
