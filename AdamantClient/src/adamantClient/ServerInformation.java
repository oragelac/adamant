package adamantClient;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * This class stores server information.
 * This information may be accessed and modified using getter and setter methods.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 */

public class ServerInformation
{
	private int _port;
	private String _hostName;

	/**
	 * Class constructor.
	 * It uses the readInformations method.
	 */

	public ServerInformation() 
	{
		readInformations();
	}

	/**
	 * This method reads the server configuration from a configuration file.
	 *
	 * @see java.nio.file.Files;
	 * @see java.nio.file.Paths;
	 */

	public void readInformations()
	{
		try 
		{
			Stream<String> lines = Files.lines(Paths.get("resources/config/server.config"));
			lines.forEach(s -> this.parse(s));
			lines.close();
		} 
		
		catch (IOException e) 
		{
			System.out.println("-- ERROR -- server.config not found");
		}
	}

	/**
	 * This method separates the config file into lines then read the information.
	 * This method uses the class setters.
	 */

	private void parse(String s) 
	{
		String[] splittedLine = s.split(" ");
		if(splittedLine.length == 2)
		{
			if(splittedLine[0].equals("host"))
			{
				setHostName(splittedLine[1]);
			}
			
			else if(splittedLine[0].equals("port"))
			{
				setPort(Integer.parseInt(splittedLine[1]));
			}
			
			else
			{
				System.out.println("Invalid parameter");
			}
		}
		
		else
		{
			System.out.println("Invalid configuration file structure");
		}
	}

	/**
	 * Port getter
	 *
	 * @return the socket's port
	 */

	public int getPort() 
	{
		return _port;
	}

	/**
	 * Port setter
	 *
	 * @param the socket's port
	 */

	public void setPort(int port) 
	{
		_port = port;
	}

	/**
	 * Hostname getter
	 *
	 * @return the hostname
	 */

	public String getHostName() 
	{
		return _hostName;
	}

	/**
	 * Hostname setter
	 *
	 * @param the hostname
	 */

	public void setHostName(String hostName) 
	{
		_hostName = hostName;
	}
	
	
}
