package adamantShared;

import java.io.Serializable;

public class Card implements Serializable
{
	private static final long serialVersionUID = 1280780053546051326L;
	private int _id, _attack, _health, _currentHealth, _cost;
	private String _name, _description, _fileName;
	
	public Card(int id, int attack, int health, int cost, String name, String description, String fileName)
	{
		_id = id;
		_attack = attack;
		_health = health;
		_cost = cost;
		_name = name;
		_description = description;
		_fileName = fileName;
		_currentHealth = _health;
	}

	public Card() 
	{}

	public boolean isAlive()
	{
		return _currentHealth > 0;
	}
	
	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}
	

	public int getAttack() {
		return _attack;
	}

	public void setAttack(int _attack) {
		this._attack = _attack;
	}

	public int getHealth() {
		return _health;
	}

	public void setHealth(int _health) 
	{
		if(_health < 0)
			_health = 0;
		this._health = _health;
	}

	public int getCurrentHealth() {
		return _currentHealth;
	}

	public void setCurrentHealth(int _currentHealth) 
	{
		if(_currentHealth < 0)
			_currentHealth = 0;
		this._currentHealth = _currentHealth;
	}

	public int getCost() {
		return _cost;
	}

	public void setCost(int _cost) {
		this._cost = _cost;
	}

	public String getName() {
		return _name;
	}

	public void setName(String _name) {
		this._name = _name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String _description) {
		this._description = _description;
	}

	public String getFileName() {
		return _fileName;
	}

	public void setFileName(String _fileName) {
		this._fileName = _fileName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _attack;
		result = prime * result + _cost;
		result = prime * result
				+ ((_description == null) ? 0 : _description.hashCode());
		result = prime * result
				+ ((_fileName == null) ? 0 : _fileName.hashCode());
		result = prime * result + _health;
		result = prime * result + _id;
		result = prime * result + ((_name == null) ? 0 : _name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (_attack != other._attack)
			return false;
		if (_cost != other._cost)
			return false;
		if (_description == null) {
			if (other._description != null)
				return false;
		} else if (!_description.equals(other._description))
			return false;
		if (_fileName == null) {
			if (other._fileName != null)
				return false;
		} else if (!_fileName.equals(other._fileName))
			return false;
		if (_health != other._health)
			return false;
		if (_id != other._id)
			return false;
		if (_name == null) {
			if (other._name != null)
				return false;
		} else if (!_name.equals(other._name))
			return false;
		return true;
	}	
}
