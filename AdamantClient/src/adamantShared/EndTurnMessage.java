package adamantShared;

public class EndTurnMessage extends Message
{
	private static final long serialVersionUID = 8473489591129834449L;

	public EndTurnMessage()
	{
        _action = "EndTurn";
    }
}
