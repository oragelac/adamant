package adamantShared;

import java.io.Serializable;

public class Message implements Serializable {

	private static final long serialVersionUID = -6247891731024276716L;
	protected String _action;

	public String getAction() {
		return _action;
	}
	public void setAction(String _action) {
		this._action = _action;
	}
}
