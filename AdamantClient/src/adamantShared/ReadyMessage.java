package adamantShared;

public class ReadyMessage extends Message 
{
	private static final long serialVersionUID = 7307763535315064287L;

	public ReadyMessage()
	{
		_action = "Ready";
	}
}
