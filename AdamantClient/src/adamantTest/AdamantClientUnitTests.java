package adamantTest;

import static org.junit.Assert.*;
import org.junit.Test;
import adamantClient.ClientInformation;

public class AdamantClientUnitTests
{
	@Test
	public void testClientInformation()
	{
		//Creating object to perform tests
		ClientInformation clientInformation = ClientInformation.getInstance();
		clientInformation.setId(0);
		clientInformation.setName("name");
		clientInformation.setEnemyName("enemyName");
		clientInformation.setDeckChoice(0);
		clientInformation.setEnemyDeckChoice(1);
		
		//Testing getters
		assertEquals(clientInformation.getId(), 0);
		assertEquals(clientInformation.getName(), "name");
		assertEquals(clientInformation.getEnemyName(), "enemyName");
		assertEquals(clientInformation.getDeckChoice(), 0);
		assertEquals(clientInformation.getEnemyDeckChoice(), 1);
		
	}
}
