package adamantServer;

import java.util.AbstractCollection;
import java.util.ArrayList;
import adamantShared.Card;

/**
 * This is a shared class maintaining the state of the board card set. 
 * It implements the CardSet interface.
 * 
 * @see		CardSet
 * @see		java.util.AbstractCollection
 * @see		java.util.ArrayList
 * @see		adamantShared.Card
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public class Board implements CardSet
{
	private ArrayList<Card> _cards;
	
	/**
     * Main constructor. 
    */
	
	public Board()
	{
		_cards = new ArrayList<Card>();
	}
	
	/**
     * This is the cards getter method. 
     * 
     * @return	contained cards.
    */
	
	@Override
	public AbstractCollection<Card> getCards() 
	{
		return _cards;
	}

	/**
     * This is the cards setter method. 
     * 
     * @param	cards collection.
    */
	
	@Override
	public void setCards(AbstractCollection<Card> cards) 
	{
		_cards = (ArrayList<Card>) cards;
	}

	/**
     * This method appends a card in the set. 
     * 
     * @param	card to append.
    */
	
	@Override
	public void appendCard(Card card) 
	{
		_cards.add(card);
		
	}
	
	/**
     * This method removes a card from the set. 
     * 
     * @param	card to remove.
    */

	@Override
	public void removeCard(Card card) 
	{
		_cards.remove(card);
	}
	
	/**
     * This method removes a card from the set. 
     * 
     * @param	position of the card to remove.
    */
	
	@Override
	public void removeCard(int position) 
	{
		_cards.remove(position);
	}
	
	/**
     * This method returns the cards set size. 
     * 
     * @param	cards set size.
    */

	@Override
	public int size() 
	{
		return _cards.size();
	}	
	
	/**
     * This method indicates if the card is contained in the cards set or not. 
     * 
     * @param	card to check.
     * @return	the card presence in the set.
    */
	
	@Override
	public boolean contains(Card card)
	{
		return _cards.contains(card);
	}
	
	/**
     * This method indicates if the cards set is empty or not. 
     * 
     * @return	the cards set state.
    */
	
	@Override
	public boolean isEmpty() 
	{
		return _cards.isEmpty();
	}
}
