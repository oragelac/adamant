package adamantServer;

import java.util.AbstractCollection;
import adamantShared.Card;

/**
 * This is an interface providing all methods that a card set must implement. 
 * 
 * @see		java.util.AbstractCollection
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public interface CardSet
{
	public AbstractCollection<Card> getCards();
	public void setCards(AbstractCollection<Card> cards);
	public void appendCard(Card card);
	public void removeCard(Card card);
	public void removeCard(int position);
	public int size();
	public boolean contains(Card card);
	public boolean isEmpty();
}
