package adamantServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import adamantShared.Message;


/**
 * This class maintains a connection to the clients and manages input/output streams.
 * 
 * @see		java.net.Socket
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public class ConnectionManager
{
	private int _port;
	private ServerSocket _socket;
	private Socket[] _clientSockets;
	ObjectOutputStream[] _outToClients;
	ObjectInputStream[] _inFromClients;
	ObjectOutputStream _outToClient;
	ObjectInputStream _inFromClient;
	private boolean _connected;
	
	/**
	 * Main Constructor.
	 * 
	 * @param the port that the server must listen for connections on.
	*/
	
	ConnectionManager(int port)
	{
		_port = port;
		_clientSockets = new Socket[2];
		_outToClients = new ObjectOutputStream[2];
		_inFromClients = new ObjectInputStream[2];
		_connected = false;
		
		try 
		{
			_socket = new ServerSocket(_port);
			_connected = true;
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * This method waits for clients to connect and accepts connections.
	 * It then initializes output and intput streams.
	*/
	
	public void acceptClients()
	{
		try 
		{	
			for(int i = 0; i < 2; ++i)
			{
				System.out.println("-- CM:INFO -- Waiting for clients...");
				_clientSockets[i] = _socket.accept();
				System.out.println("-- CM:INFO -- A client has been accepted.");
				_outToClients[i] = new ObjectOutputStream(_clientSockets[i].getOutputStream());
				_inFromClients[i] = new ObjectInputStream(_clientSockets[i].getInputStream());
			}
			
			System.out.println("-- CM:INFO -- All clients have been accepted !");
			_socket.close();
		} 
		
		catch (IOException e) 
		{
			System.out.println("-- CM:ERROR 0 -- Client stream not initialized");
		}
	}
	
	/**
	 * This method waits for a message from a client given its id.
	 * This method is blocking.
	 * 
	 * @param the client id.
	 * @return the message received from the client.
	*/
	
	public Message receive(int clientId)
	{
		try 
		{
			return (Message)_inFromClients[clientId].readObject();
		} 
		
		catch (IOException e)
		{
			System.out.println("-- CM:ERROR 2 -- Client " + clientId + " is unreachable");
		}

		catch (ClassNotFoundException e )
		{
			System.out.println("-- CM:ERROR 3 -- Message type unrecognized");
		}
		
		return new Message();
	}
	
	/**
	 * This method sends a message to a client given its id.
	 * 
	 * @param the client id.
	 * @param the message to be transferred to the client.
	*/
	
	public void send(int playerId, Message message)
	{
		try 
		{
			
				_outToClients[playerId].writeObject(message);
		} 
		
		catch (IOException e) 
		{
			System.out.println("-- CM:ERROR 4 -- Message not sent - Client unreachable ?");
		}

		try 
		{
			_outToClients[playerId].reset();
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	/**
	 * This method sends a message to all clients.
	 * 
	 * @param the message to be transferred to the clients.
	*/
	
	public void send(Message[] messages)
	{
		try 
		{
			for(int i = 0; i < 2; ++i)
			{
				_outToClients[i].writeObject(messages[i]);
			}
		} 
		
		catch (IOException e) 
		{
			System.out.println("-- CM:ERROR 4 -- Message not sent - Client unreachable ?");
		}
		
		for(int i = 0; i < 2; ++i)
		{
			try 
			{
				_outToClients[i].reset();
			} 
			
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * This method waits for a message from all clients.
	 * This method is blocking.
	 * 
	 * @return the messages received from the client as an array.
	*/
	
	public Message[] receive()
	{
		Message[] messages = new Message[2];
		try 
		{
			for(int i = 0; i < 2; ++i)
				messages[i] = (Message)_inFromClients[i].readObject();
			
			return messages;
		} 
		
		catch (IOException e)
		{
			System.out.println("-- CM:ERROR 5 -- Message not received - Client unreachable ?");
		}

		catch (ClassNotFoundException e)
		{
			System.out.println("-- CM:ERROR 6 -- Message type unrecognized");
		}
		
		return new Message[2];
	}
	
	/**
	 * This method closes the connections with the clients.
	*/
	
	public void close()
	{
		try 
		{		
			_clientSockets[0].close();
			_clientSockets[1].close();
		} 
		
		catch (IOException e) 
		{
			System.out.println("-- CM:WARNING -- Connexions do not exist");
		}
	}
	
	/**
	 * This method returns the connection state.
	 * 
	 * @return return true is connection is alive, false if it is not.
	*/
	
	public boolean isConnectionEstablished()
	{
		System.out.println("-- CM:INFO -- TCP Interface initialized");
		return _connected;
	}
}