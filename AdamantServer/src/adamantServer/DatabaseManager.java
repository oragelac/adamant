package adamantServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import adamantShared.Card;

/**
 * This class manages requests and maintains a connection to the SQlite database.
 * It requires the JDBC driver to work properly. 
 * 
 * @see		java.sql.Connection
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public class DatabaseManager
{
	Connection _connection;
	
	/**
     * Main constructor. 
    */
	
	public DatabaseManager()
	{
		try 
		{
			Class.forName("org.sqlite.JDBC");
			_connection = DriverManager.getConnection("jdbc:sqlite:resources/adamant.sqlite");
		} 

		catch (ClassNotFoundException e)
		{
			System.out.println("-- DBM:ERROR 0 -- SQL Library not found.");
		}

		catch (SQLException e) 
		{
			System.out.println("-- DBM:ERROR 1 -- Database not found.");
		}
	}
	
	/**
     * This methods execute a request on the database to retrieve the number of cards stored in the database.
     * 
     *  @return number of cards in the database.
    */
	
	public int getNumberOfCards()
	{
		System.out.println("-- DBM:INFO -- Counting cards in database");

		try 
		{
			Statement statement = _connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT COUNT(*) AS numberOfCards FROM Card");
			
			if(rs.next())
			{
				int numberOfCards = rs.getInt("numberOfCards");
				
				return numberOfCards;
			}
		} 
		
		catch (SQLException e) 
		{
			System.out.println("-- DBM:ERROR 2 -- Wrong SQL REQUEST");
		}
		
		return 0;
	}
	
	/**
     * This methods execute a request on the database to retrieve the number of decks stored in the database.
     * 
     * @return number of decks in the database.
    */

	public int getNumberOfDecks()
	{
		System.out.println("-- DBM:INFO -- Counting decks in database");

		try 
		{
			Statement statement = _connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT COUNT(*) AS numberOfDecks FROM Deck");
			
			if(rs.next())
			{
				int numberOfDecks = rs.getInt("numberOfDecks");
				
				return numberOfDecks;
			}
		} 
		
		catch (SQLException e) 
		{
			System.out.println("-- DBM:ERROR 2 -- Wrong SQL REQUEST");
		}
		
		return 0;
	}
	
	/**
     * This methods execute a request on the database to create a deck given an id.
     * 
     * @param deck id.
     * @return the created deck.
    */
	
	public Deck getDeckFromId(int id)
	{
		System.out.println("-- DBM:INFO -- Loading deck from database");

		try 
		{
			Statement statement = _connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM Deck WHERE id = " + id);
			
			if(rs.next())
			{
				String name = rs.getString("name");
				String description = rs.getString("description");
				String containedCards = rs.getString("cards");
				
				Deck deck = new Deck(id, name, description);
				for(char character : containedCards.toCharArray())
				{
					int cardId = Character.getNumericValue(character);
					deck.appendCard(getCardById(cardId));
					
				}
				
				return deck;
			}
		} 
		
		catch (SQLException e) 
		{
			System.out.println("-- DBM:ERROR 2 -- Wrong SQL REQUEST");
		}
		
		return new Deck();
	}
	
	/**
     * This methods execute a request on the database to create a card given an id.
     * 
     * @param card id.
     * @return the created card.
    */
	
	public Card getCardById(int id)
	{
		System.out.println("-- DBM:INFO -- Loading card " + id + " from database");
		try 
		{
			Statement statement = _connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM Card WHERE id = " + id);
			
			if(rs.next())
			{
				return new Card(rs.getInt("id"), rs.getInt("attack"), rs.getInt("health"), rs.getInt("cost"), rs.getString("name"), rs.getString("description"), rs.getString("fileName"));
			}
		} 
		
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return new Card();
	}
	
	/**
     * This methods execute a request on the database to retrieve deck names.
     * 
     * @see ArrayList
     * @return the deck names as an ArrayList<String>.
    */

	public ArrayList<String> getDeckNames()
	{
		System.out.println("-- DBM:INFO -- Loading deck names from database");

		ArrayList<String> names = new ArrayList<String>();
		
		try 
		{
			Statement statement = _connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT name FROM Deck");
			
			while(rs.next())
			{
				names.add(rs.getString("name"));
			}
		} 
		
		catch (SQLException e) 
		{
			System.out.println("-- DBM:ERROR 3 -- Wrong SQL REQUEST");
		}
		
		return names;
	}
	
	/**
     * This methods execute a request on the database to retrieve deck descriptions.
     * 
     * @see ArrayList
     * @return the deck descriptions as an ArrayList<String>.
    */
	
	public ArrayList<String> getDeckDescriptions()
	{
		System.out.println("-- DBM:INFO -- Loading deck description from database");

		ArrayList<String> descriptions = new ArrayList<String>();
		
		try 
		{
			Statement statement = _connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT description FROM Deck");
			
			while(rs.next())
			{
				descriptions.add(rs.getString("description"));
			}
		} 
		
		catch (SQLException e) 
		{
			System.out.println("-- DBM:ERROR 4 -- Wrong SQL REQUEST");
		}
		
		return descriptions;
	}
}
