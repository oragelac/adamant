package adamantServer;

import java.util.AbstractCollection;
import java.util.Collections;
import java.util.Stack;

import adamantShared.Card;

/**
 * This is a shared class maintaining the state of the deck card set. 
 * It implements the CardSet interface.
 * 
 * @see		CardSet
 * @see		java.util.AbstractCollection
 * @see		java.util.Collections
 * @see		java.util.Stack
 * @see		adamantShared.Card
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public class Deck implements CardSet
{
	private Stack<Card> _cards;
	private String _name, _description;
	private int _id;
	
	/**
     * Main constructor. 
    */
	
	public Deck()
	{
		_cards = new Stack<Card>();
	}
	
	public Deck(int id, String name, String description)
	{
		_id = id;
		_name = name;
		_description = description;
		_cards = new Stack<Card>();
	}

	/**
     * This is the cards getter method. 
     * 
     * @return	contained cards.
    */
	
	@Override
	public AbstractCollection<Card> getCards() 
	{
		return _cards;
	}

	/**
     * This is the cards setter method. 
     * 
     * @param	cards collection.
    */
	
	@Override
	public void setCards(AbstractCollection<Card> cards) 
	{
		_cards = (Stack<Card>) cards;
	}

	/**
     * This method appends a card in the set. 
     * 
     * @param	card to append.
    */
	
	@Override
	public void appendCard(Card card) 
	{
		_cards.push(card);	
	}

	/**
     * This method removes a card from the set. 
     * 
     * @param	card to remove.
    */
	
	@Override
	public void removeCard(Card card) 
	{
		_cards.remove(card);
	}
	
	/**
     * This method removes a card from the set. 
     * 
     * @param	position of the card to remove.
    */
	
	@Override
	public void removeCard(int position) 
	{
		_cards.remove(position);
	}
	
	/**
     * This method returns the card at the top of the deck. 
     * It calls the Stack.pop() method.
     * 
     * @return	card at the top of the deck.
    */
	
	public Card pickCard() 
	{
		return _cards.pop();
	}
	
	/**
     * This method shuffles the deck.
     * 
    */
	
	public void shuffle()
	{
		Collections.shuffle(_cards);
	}
	
	/**
     * This method returns the cards set size. 
     * 
     * @param	cards set size.
    */
	
	@Override
	public int size() 
	{
		return _cards.size();
	}
	
	/**
     * This method indicates if the card is contained in the cards set or not. 
     * 
     * @param	card to check.
     * @return	the card presence in the set.
    */
	
	@Override
	public boolean contains(Card card)
	{
		return _cards.contains(card);
	}
	
	/**
     * This is the deck name getter method. 
     * 
     * @return	deck name.
    */
	
	public String getName()
	{
		return _name;
	}
	
	/**
     * This is the description getter method. 
     * 
     * @return	deck description.
    */
	
	public String getDescription()
	{
		return _description;
	}
	
	/**
     * This is the id getter method. 
     * 
     * @return	deck id.
    */
	
	public int getId()
	{
		return _id;
	}
	
	/**
     * This method indicates if the cards set is empty or not. 
     * 
     * @return	the cards set state.
    */

	@Override
	public boolean isEmpty() 
	{
		return _cards.isEmpty();
	}
}
