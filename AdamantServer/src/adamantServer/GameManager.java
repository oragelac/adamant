package adamantServer;
import java.util.ArrayList;

import adamantShared.*;

/**
 * This class stores the current game state and 
 * creates messages that will be transmitted to the clients.
 * 
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
*/

public class GameManager 
{
	private GameState _gameState;
	private boolean _quit;
	
	/**
     * Main constructor. 
    */
	
	public GameManager()
	{
		_gameState = new GameState();
		_quit = false;
	}
	
	/**
     * This method generates an array of two SalutationMessages containing players names.
     *  
     *  @return the generated messages stored in an array.
    */
	
	public SalutationMessage[] generateSalutationMessages() 
	{
		SalutationMessage[] messages = new SalutationMessage[2];
		
		for (int i = 0; i < 2; ++i)
			messages[i] = new SalutationMessage(_gameState.getPlayerNames()[1-i]);
		
		return messages;
	}
	
	/**
     * This method generates an array of two SalutationMessages containing players ids.
     *  
     *  @return the generated messages stored in an array.
    */
	
	public SalutationMessage[] generateHiMessages()
	{
		SalutationMessage[] messages = new SalutationMessage[2];
		
		for (int i = 0; i < 2; ++i)
			messages[i] = new SalutationMessage(i);
		
		return messages;
	}
	
	/**
     * This method generates an array of two ReadyMessages
     *  
     *  @return the generated messages stored in an array.
    */
	
	public ReadyMessage[] generateReadyMessages()
	{
		ReadyMessage[] messages = new ReadyMessage[2];
		
		for (int i = 0; i < 2; ++i)
			messages[i] = new ReadyMessage();
		
		return messages;
	}
	
	/**
     * This method generates an array of two DeckChoiceMessages with decks names and descriptions.
     *  
     *  @return the generated messages stored in an array.
    */
	
	public DeckChoiceMessage[] generateDeckChoiceMessages()
	{
		DeckChoiceMessage[] messages = new DeckChoiceMessage[2];
		for (int i = 0; i < 2; ++i)
			messages[i] = new DeckChoiceMessage(_gameState.getDeckNames(), _gameState.getDeckDescriptions());
		
		return messages;
	}
	
	/**
     * This method generates an array of two DeckChoicenMessages with the id of the deck selected by the enemy.
     *  
     *  @return the generated messages stored in an array.
    */
	
	public Message[] generateEnemyDeckChoiceMessages()
	{
		DeckChoiceMessage[] messages = new DeckChoiceMessage[2];
		for (int i = 0; i < 2; ++i)
			messages[i] = new DeckChoiceMessage(_gameState.getDecks()[1-i].getId());
		
		return messages;
	}
	
	/**
     * This method generates an array of two RulesMessages to inform player of the rules of the current game.
     *  
     *  @return the generated messages stored in an array.
    */
	
	public RulesMessage[] generateRulesMessages() 
	{
		RulesMessage[] messages = new RulesMessage[2];
		for (int i = 0; i < 2; ++i)
			messages[i] = new RulesMessage(_gameState.getNumberOfCards(), _gameState.getNumberOfDecks(), _gameState.getMaximumNumberOfCardsOnBoard(), _gameState.getMaximumNumberOfCardsInHand());
		
		return messages;
	}
	
	/**
     * This method generates an array of two UpdateMessages.
     * These messages contain the current state of the game 
     * from the point of view of a specific player.
     *  
     *  @return the generated messages stored in an array.
    */
	
	public UpdateMessage[] generateUpdateMessages()
	{
		UpdateMessage[] messages = new UpdateMessage[2];
		for (int i = 0; i < 2; ++i)
		{
			if(_gameState.getValidLastAction())
			{
				messages[i] = new UpdateMessage(_gameState.getLastAction(),
												_gameState.isActive(i),
												_gameState.getTurn(),
												_gameState.getActionPoints()[i],
												_gameState.getActionPoints()[1-i],
												_gameState.getHands()[1-i].getCards().size(),
												_gameState.getDecks()[i].getCards().size(),
												(ArrayList<Card>)_gameState.getHands()[i].getCards(),
												(ArrayList<Card>)_gameState.getBoards()[i].getCards(),
												(ArrayList<Card>)_gameState.getBoards()[1-i].getCards());
			}
			
			else
			{
				messages[i] = new UpdateMessage(_gameState.isActive(i),
												_gameState.getTurn(),
												_gameState.getActionPoints()[i],
												_gameState.getActionPoints()[1-i],
												_gameState.getHands()[1-i].getCards().size(),
												_gameState.getDecks()[i].getCards().size(),
												(ArrayList<Card>)_gameState.getHands()[i].getCards(),
												(ArrayList<Card>)_gameState.getBoards()[i].getCards(),
												(ArrayList<Card>)_gameState.getBoards()[1-i].getCards());				
			}
		}
		
		return messages;
	}
	
	/**
     * This method generates an array of two GameOverMessages that will be sent to clients when the game is over.
     *  
     *  @return the generated messages stored in an array.
    */
	
	public GameOverMessage[] generateGameOverMessages()
	{
		GameOverMessage[] messages = new GameOverMessage[2];
		for (int i = 0; i < 2; ++i)
			messages[i] = new GameOverMessage(_gameState.getWinnerId());
		
		return messages;
	}
	
	/**
     * This method updates the game state using a message received from a client.
     * 
     *  
     *  @param the id of the player that sent the message.
     *  @param the message received from the client.
    */

	public void updateGameState(int playerId, Message message)
	{
		System.out.println("-- GM:INFO -- Message received from player #" + playerId + " ; action = " + message.getAction());
		
		// PreGame messages
		if(message instanceof SalutationMessage)
		{
			_gameState.setPlayerName(playerId, ((SalutationMessage)message).getNickname());
		}

		else if(message instanceof DeckChoiceMessage)
		{
			_gameState.createDeck(playerId, ((DeckChoiceMessage)message).getDeckId());
		}

		else if(message instanceof ReadyMessage)
		{
			_gameState.deal(playerId, _gameState.getNumberOfCardsInHandAtStart());
		}

		// Game messages
		else if(message instanceof PlayCardMessage)
		{
			_gameState.playCard(playerId, ((PlayCardMessage)message).getSource());
		}

		else if(message instanceof AttackMessage)
		{
			_gameState.attack(playerId, ((AttackMessage)message).getSource(), ((AttackMessage)message).getDestination());
		}

		else if(message instanceof PickCardMessage)
		{
			_gameState.pickCard(playerId);
		}

		else if(message instanceof EndTurnMessage)
		{
			_gameState.nextTurn();
		}

		else if(message instanceof ExitMessage)
		{
			setQuit(true);
		}

		else
		{
			System.out.println("-- GM:INFO -- Wrong action : " + message.getAction());
		}
		
		_gameState.cleanBoards();
		_gameState.isGameOver();
	}
	
	/**
     * This is a setter method.
     * When this method is called with the argument 'true', 
     * the current game ends and the server restarts.
     *  
     *  @param the value of quit.
    */
	
	private void setQuit(boolean quit) 
	{
		_quit = quit;
		
	}
	
	/**
     * This is a getter method.
     * It returns the state of the quit attribute.
     *  
     *  @return the state of the quit attribute.
    */
	
	public boolean getQuit()
	{
		return _quit;
	}

	/**
     * This method indicates if the current game is over or not.
     *  
     *  @return the state of the current game.
    */
	
	boolean isGameOver()
	{
		return _gameState.getGameOver();
	}
	
	/**
     * This method returns the current active player during this turn.
     *  
     *  @return the active player.
    */
	
	int getActivePlayer()
	{
		return _gameState.getActivePlayer();
	}
}
