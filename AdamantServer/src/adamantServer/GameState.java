package adamantServer;

import java.util.ArrayList;
import adamantShared.Card;

/**
 * This class holds all the mechanics, rules, states and information of the game.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 *
 * @see adamantShared.Card;
 * @see adamantServer.Hand;
 * @see adamantServer.Deck;
 * @see adamantServer.Board;
 * @see adamantServer.DatabaseManager;
 * @see adamantServer.GameManager;
 */

public class GameState
{
	private Deck[] _decks;
	private Hand[] _hands;
	private Board[] _boards;
	private int _activePlayer, _turn, _winnerId;
	private int[] _actionPoints;
	private boolean _gameOver, _validLastAction;
	private DatabaseManager _databaseManager;
	private String _lastAction, _playerNames[];
	private int _maximumNumberOfCardsOnBoard, _maximumNumberOfCardsInHand, _numberOfCardsInHandAtStart;

	/**
	 * Class Constructor.
	 */

	public GameState()
	{
		_gameOver = false;
		_activePlayer = 0;
		_turn = 1;
		_decks = new Deck[2];
		_hands = new Hand[2];
		_boards = new Board[2];
		_actionPoints = new int[2];
		_playerNames = new String[2];
		_databaseManager = new DatabaseManager();
		_winnerId = -1;
		
		_numberOfCardsInHandAtStart = 5;
		_maximumNumberOfCardsOnBoard = 8;
		_maximumNumberOfCardsInHand = 8;
		
		
		for(int i = 0; i < 2; ++i)
		{
			_hands[i] = new Hand();
			_boards[i] = new Board();
			_actionPoints[i] = 2;
		}	
	}

	/**
	 * Deck loading method. It loads then shuffle the player's deck.
	 *
	 * @see adamantServer.DatabaseManager;
	 * @param playerId;
	 * @param deckId;
	 */

	public void createDeck(int playerId, int deckId) 
	{
		_decks[playerId] = _databaseManager.getDeckFromId(deckId); 
		_decks[playerId].shuffle();
	}

	/**
	 * Deck dealing method, it sends the requested amount of cards to the player.
	 *
	 * @see adamantServer.Hand
	 * @see adamantServer.Deck
	 * @param playerId of the player
	 * @param amountOfCardsToDeal;
	 */

	public boolean deal(int playerId, int amountOfCardsToDeal)
	{
		if(_decks[playerId].isEmpty() || _hands[playerId].size() + amountOfCardsToDeal > _maximumNumberOfCardsOnBoard)
			return false;
		
		for(int i = 0; i < amountOfCardsToDeal; ++i)
			_hands[playerId].appendCard(_decks[playerId].pickCard());
		
		return true;
	}

	/**
	 * Next Turn method. Makes the game pass to the next turn.
	 */

	public void nextTurn()
	{
		_validLastAction = false;
		_turn += 1;
		switchPlayer();
		int bonusPoints = 1;
		System.out.println(_actionPoints[_activePlayer]);
		_actionPoints[_activePlayer] += 2 * (_turn / 3);
		System.out.println(_actionPoints[_activePlayer]);
		
		if(_turn == 2 || (_turn >= 6 && !_boards[_activePlayer].isEmpty()))
		{
			_actionPoints[_activePlayer] += bonusPoints;
		}		
	}

	/**
	 * PlayerName setter.
	 *
	 * @param playerId of the corresponding player.
	 * @param name of the player.
	 */

	public void setPlayerName(int playerId, String name)
	{
		_playerNames[playerId] = name;
	}

	/**
	 * PlayerName getter.
	 *
	 * @return the player name.
	 */

	public String[] getPlayerNames()
	{
		return _playerNames;
	}

	/**
	 * Decks getter.
	 *
	 * @return the card array.
	 */

	public Deck[] getDecks()
	{
		return _decks;
	}

	/**
	 * Decks setter.
	 *
	 * @see adamantServer.Deck
	 * @param the deck array.
	 */

	public void setDecks(Deck[] _decks)
	{
		this._decks = _decks;
	}

	/**
	 * Hands getter.
	 *
	 * @see adamantServer.Hand
	 * @return the hand array.
	 */


	public Hand[] getHands()
	{
		return _hands;
	}

	/**
	 * Decks setter.
	 *
	 * @see adamantServer.Hand
	 * @param the hand array.
	 */

	public void setHands(Hand[] _hands)
	{
		this._hands = _hands;
	}

	/**
	 * ActivePlayer getter.
	 *
	 * @return the active player.
	 */

	public int getActivePlayer()
	{
		return _activePlayer;
	}

	/**
	 * ActivePlayer setter.
	 *
	 * @param the active player.
	 */

	public void setActivePlayer(int _activePlayer)
	{
		this._activePlayer = _activePlayer;
	}

	/**
	 * Turn getter.
	 *
	 * @return the active turn.
	 */

	public int getTurn()
	{
		return _turn;
	}

	/**
	 * Winner ID getter.
	 *
	 * @return the winner.
	 */

	public int getWinnerId() 
	{
		return _winnerId;
	}

	/**
	 * Boards getter.
	 *
	 * @return the board array.
	 */

	public Board[] getBoards() 
	{
		return _boards;
	}

	/**
	 * Boards getter.
	 *
	 * @param the board array.
	 */

	public void setBoard(Board[] _boards)
	{
		this._boards = _boards;
	}

	/**
	 * Game Over "complex" setter, sets the game as over if it is actually over.
	 */

	public void isGameOver() 
	{
		if(_decks[0] != null && 
			_decks[1] != null && 
			_boards[0] != null && 
			_boards[1] != null && 
			_hands[0] != null && 
			_hands[1] != null)
		{
			for(int i = 0; i < 2; ++i)
			{
				if(_decks[i].isEmpty() && _hands[i].isEmpty())
				{
					_winnerId = 1 -i;
					
					if(!_boards[i].isEmpty())
					{
						_gameOver = true;
						
						for(Card card : _boards[i].getCards())
						{
							if(card.isAlive())
							{
								_gameOver = false;
								_winnerId = -1;
							}
						}
					}
					
					else
					{
						_gameOver = true;
					}
				}
			}
		}
	}

	/**
	 * Game Over getter.
	 *
	 * @return the Game Over state.
	 */
	
	public boolean getGameOver()
	{
		return _gameOver;
	}

	/**
	 * Game Over setter
	 *
	 * @param the Game status (Over or not)
	 */


	public void setGameOver(boolean gameOver)
	{
		_gameOver = gameOver;
	}

	/**
	 * Play Card method, it checks the necessary conditions then do the play card operation.
	 *
	 * @see adamantServer.Hand
	 * @see adamantServer.Board
	 * @param the id of the player
	 * @param the position of the card
	 */

	public void playCard(int playerId, int position) 
	{
		Card card = ((ArrayList<Card>)_hands[playerId].getCards()).get(position);
		
		if(_actionPoints[playerId] >= card.getCost() && _boards[playerId].size() < _maximumNumberOfCardsOnBoard)
		{
			_hands[playerId].removeCard(position);
			_boards[playerId].appendCard(card);
			_actionPoints[playerId] -= card.getCost();
			_lastAction = "played a card.";
			_validLastAction = true;
		}
		
		else
			_validLastAction = false;
	}

	/**
	 * Pick Card method, pick a card for the player if he can.
	 *
	 * @param the id of the player
	 */

	public void pickCard(int playerId)
	{
		if(_actionPoints[playerId] >= 1)
		{
			if(deal(playerId, 1))
			{
				--_actionPoints[playerId];
				_lastAction = "picked a card.";
				_validLastAction = true;
			}
			
			else
				_validLastAction = false;
		}
		
		else
			_validLastAction = false;
	}

	/**
	 * Attack method, if the player has enough action points
	 *
	 * @see adamantServer.Board
	 * @param the id of the player.
	 * @param the position of the card on the board.
	 * @param the position of the enemy card on the board.
	 */

	public void attack(int playerId, int positionOnBoard, int positionOnEnemyBoard)
	{
		Card sourceCard = ((ArrayList<Card>)_boards[playerId].getCards()).get(positionOnBoard);
		
		if(_actionPoints[playerId] >= sourceCard.getCost())
		{
			Card destinationCard = ((ArrayList<Card>)_boards[1-playerId].getCards()).get(positionOnEnemyBoard);
			int enemyHealthLeft = destinationCard.getCurrentHealth() - sourceCard.getAttack();
			int healthLeft = sourceCard.getCurrentHealth() - destinationCard.getAttack();
			destinationCard.setCurrentHealth(enemyHealthLeft);
			sourceCard.setCurrentHealth(healthLeft);
			((ArrayList<Card>)_boards[1-playerId].getCards()).set(positionOnEnemyBoard, destinationCard);
			((ArrayList<Card>)_boards[playerId].getCards()).set(positionOnBoard, sourceCard);
			--_actionPoints[playerId];
			_lastAction = "attacked a card.";
			_validLastAction = true;
		}
		
		else
			_validLastAction = false;
	}

	/**
	 * Switch Player method, switch the active player.
	 */

	private void switchPlayer()
	{
		_activePlayer = 1 -_activePlayer;
	}

	/**
	 * Action Points getter
	 *
	 * @return the action points
	 */

	public int[] getActionPoints()
	{
		return _actionPoints;
	}

	/**
	 * This method sets the player as active then return the player.
	 *
	 * @param id of the player.
	 * @return id of the player.
	 */

	public boolean isActive(int id)
	{
		return (_activePlayer == id);
	}

	/**
	 * This method clears the game boards.
	 */

	public void cleanBoards()
	{
		if(_boards[0] != null && _boards[1] != null)
		{
			for(int i = 0; i < 2; ++i)
				((ArrayList<Card>)_boards[i].getCards()).removeIf(card -> !card.isAlive());
		}
	}

	/**
	 * Deck names getter
	 *
	 * @return the deck's names
	 */
	
	public ArrayList<String> getDeckNames()
	{
		return _databaseManager.getDeckNames();
	}

	/**
	 * Deck descriptions getter
	 *
	 * @return the deck's descriptions
	 */

	public ArrayList<String> getDeckDescriptions()
	{
		return _databaseManager.getDeckDescriptions();
	}

	/**
	 * This method returns the total number of cards
	 *
	 * @return the total number of cards
	 */

	public int getNumberOfCards()
	{
		return _databaseManager.getNumberOfCards();
	}

	/**
	 * This method returns the total number of decks
	 *
	 * @return the total number of decks
	 */

	public int getNumberOfDecks()
	{
		return _databaseManager.getNumberOfDecks();
	}

	/**
	 * maximum number of cards on board getter
	 *
	 * @return the maximum number of cards on board
	 */
	
	public int getMaximumNumberOfCardsOnBoard()
	{
		return _maximumNumberOfCardsOnBoard;
	}

	/**
	 * maximum number of cards on board getter
	 *
	 * @return the maximum number of cards in hand
	 */

	public int getMaximumNumberOfCardsInHand()
	{
		return _maximumNumberOfCardsInHand;
	}
	
	/**
	 * number of cards at start
	 *
	 * @return the maximum number of cards at start
	 */
	
	public int getNumberOfCardsInHandAtStart()
	{
		return _numberOfCardsInHandAtStart;
	}
	
	/**
	 * last action getter
	 *
	 * @return the last action that the active player made
	 */
	
	public String getLastAction()
	{
		return _lastAction;
	}
	
	/**
	 * last action validity getter
	 *
	 * @return the state of the last action that the active player made
	 */
	
	public boolean getValidLastAction()
	{
		return _validLastAction;
	}
}
