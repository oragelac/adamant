package adamantServer;

import adamantShared.Message;

/**
 * This class is the entry point of the server application.
 * It initializes the main network-related classes.
 *
 * @author  Antoine Calegaro
 * @author  Florian Indot
 *
 * @see adamantServer.ConnectionManager;
 * @see adamantServer.GameManager;
 */

public class ServerManager 
{
	/**
	 * This is the main method.
	 * It prevents the server from shutting down after a client exit.
	 *
	 * @param	args
	 */

	public static void main(String[] args) 
	{
		while(true)
		{
			ServerManager _manager = new ServerManager();
			_manager.run();
		}
	}
	
	private ConnectionManager _connectionManager;
	private GameManager _gameManager;

	/**
	 * Class Constructor.
	 * It initializes vital components of the server structure.
	 *
	 * @see adamantServer.ConnectionManager;
	 * @see adamantServer.GameManager;
	 */

	public ServerManager()
	{
		_connectionManager = new ConnectionManager(55555);
		_gameManager = new GameManager();
	}

	/**
	 * This method is the start point of the server lifecycle.
	 * It initializes the server-client connection.
	 *
	 * @see adamantServer.ConnectionManager;
	 * @see adamantShared.Message;
	 */

	public void run()
	{
		System.out.println("Server initializing...");

		if(_connectionManager.isConnectionEstablished())
		{
			// Waiting for the clients salutations
			Message[] messages;
			_connectionManager.acceptClients();
			// Say "Hi" to the clients
			_connectionManager.send(_gameManager.generateHiMessages());
			
			messages = _connectionManager.receive();
					
			for(int i = 0; i < 2; ++i)
				_gameManager.updateGameState(i, messages[i]);
			
			if(_gameManager.getQuit())
			{
				quit();
				return;
			}

			_connectionManager.send(_gameManager.generateSalutationMessages());

			// Then ask them to pick a deck
			_connectionManager.send(_gameManager.generateDeckChoiceMessages());
			messages = _connectionManager.receive();
			
			for(int i = 0; i < 2; ++i)
				_gameManager.updateGameState(i, messages[i]);
			
			if(_gameManager.getQuit())
			{
				quit();
				return;
			}

			messages = _connectionManager.receive();
			
			for (int i = 0; i < 2; ++i)
				_gameManager.updateGameState(i, messages[i]);
			
			if(_gameManager.getQuit())
			{
				quit();
				return;
			}
			
			_connectionManager.send(_gameManager.generateReadyMessages());
			_connectionManager.send(_gameManager.generateEnemyDeckChoiceMessages());
			_connectionManager.send(_gameManager.generateRulesMessages());

			while(!_gameManager.isGameOver())
			{
				_connectionManager.send(_gameManager.generateUpdateMessages());
				Message message = _connectionManager.receive(_gameManager.getActivePlayer());
				
				_gameManager.updateGameState(_gameManager.getActivePlayer(), message);
				
				if(_gameManager.getQuit())
				{
					quit();
					return;
				}
			}
			
			_connectionManager.send(_gameManager.generateGameOverMessages());
			quit();
		}
	}

	/**
	 * This method closes the Connection Manager and shutdowns the server.
	 * @see ConnectionManager;
	 */

	public void quit()
	{
		System.out.println("Server shutting down...");
		_connectionManager.close();
	}
}
