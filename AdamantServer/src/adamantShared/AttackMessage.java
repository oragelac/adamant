package adamantShared;

public class AttackMessage extends Message
{
	private static final long serialVersionUID = -4816408081374973945L;
	private int _source;
    private int _destination;

    public AttackMessage(int source, int destination)
    {
        _action = "Attack";
        _source = source;
        _destination = destination;
    }
    
    public int getSource()
	{
		return _source;
	}
	
	public int getDestination()
	{
		return _destination;
	}
}
