package adamantShared;

import java.util.ArrayList;

public class DeckChoiceMessage extends Message 
{
	private static final long serialVersionUID = -3200763468468469517L;
	private int _deckId;
	private ArrayList<String> _names, _descriptions;
	
	public DeckChoiceMessage(ArrayList<String> names, ArrayList<String> descriptions)
    {
        _action = "ChooseDeck";
        _names = names;
        _descriptions = descriptions;
    }

    public DeckChoiceMessage(int deckId)
    {
        _action = "DeckChoice";
        setDeckId(deckId);
    }

	public int getDeckId()
	{
		return _deckId;
	}

	public void setDeckId(int _deckId)
	{
		this._deckId = _deckId;
	}
	
	public ArrayList<String> getNames()
	{
		return _names;
	}
	
	public ArrayList<String> getDescriptions()
	{
		return _descriptions;
	}
}
