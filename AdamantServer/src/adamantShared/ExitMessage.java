package adamantShared;

public class ExitMessage extends Message
{
    private static final long serialVersionUID = -6664266642966642666L;

    public ExitMessage()
    {
        _action = "Exit";
    }
}