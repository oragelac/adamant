package adamantShared;

public class GameOverMessage extends Message 
{
	private static final long serialVersionUID = -8803286435339450804L;
	
	private int _winnerId;

	public GameOverMessage(int winnerId)
	{
        _action = "gameOver";
        _winnerId = winnerId;
    }
	
	public int getWinnerId()
	{
		return _winnerId;
	}
}
