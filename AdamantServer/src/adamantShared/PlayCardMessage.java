package adamantShared;

public class PlayCardMessage extends Message 
{
	private static final long serialVersionUID = 16406678207817168L;
	private int _source;
	
	public PlayCardMessage(int source)
	{
		_action = "PlayCard";
		_source = source;
	}
	
	public int getSource()
	{
		return _source;
	}
}
