package adamantShared;

public class RulesMessage extends Message 
{
	private static final long serialVersionUID = 1070315784365918751L;
	private int _numberOfCards, _numberOfDecks, _maximumNumberOfCardsOnBoard, _maximumNumberOfCardsInHand;
	
	public RulesMessage(int numberOfCards, int numberOfDecks, int maximumNumberOfCardsOnBoard, int maximumNumberOfCardsInHand)
	{
		_numberOfCards = numberOfCards;
		_maximumNumberOfCardsOnBoard = maximumNumberOfCardsOnBoard;
		_maximumNumberOfCardsInHand = maximumNumberOfCardsInHand;
		_numberOfDecks = numberOfDecks;
	}

	public int getNumberOfCards() 
	{
		return _numberOfCards;
	}
	
	public int getNumberOfDecks() 
	{
		return _numberOfDecks;
	}

	public int getMaximumNumberOfCardsOnBoard() 
	{
		return _maximumNumberOfCardsOnBoard;
	}

	public int getMaximumNumberOfCardsInHand() 
	{
		return _maximumNumberOfCardsInHand;
	}
}
