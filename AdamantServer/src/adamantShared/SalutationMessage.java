package adamantShared;

public class SalutationMessage extends Message
{
	private static final long serialVersionUID = -6396443427267494310L;
	private int _id;
	private String _nickname;

    public SalutationMessage(int id)
    {
        _action = "Hi";
        _id = id;
    }
    
    public SalutationMessage(String nickname)
    {
        _action = "Nickname";
        _nickname = nickname;
    }

    public int getId() 
    {
        return _id;
    }
    
    public String getNickname()
    {
    	return _nickname;
    }
}
