package adamantShared;

import java.util.ArrayList;

public class UpdateMessage extends Message 
{
	private static final long serialVersionUID = 1820886530513437707L;
	private boolean _active;
	private int _turn, _actionPoints, _enemyActionPoints, _enemyHandSize, _deckSize; 
	ArrayList<Card> _hand, _board, _enemyBoard;
	
	public UpdateMessage(boolean active, 
						int turn, 
						int actionPoints, 
						int enemyActionPoints,
						int enemyHandSize,
						int deckSize,
						ArrayList<Card> hand,
						ArrayList<Card> board,
						ArrayList<Card> enemyBoard)
	{
		_action = "";
		_active = active;
		_turn = turn;
		_actionPoints = actionPoints;
		_enemyActionPoints = enemyActionPoints;
		_enemyHandSize = enemyHandSize;
		_deckSize = deckSize;
		_hand = hand;
		_board = board;
		_enemyBoard = enemyBoard;
	}
	
	public UpdateMessage(String lastAction,
						boolean active, 
						int turn, 
						int actionPoints, 
						int enemyActionPoints,
						int enemyHandSize,
						int deckSize,
						ArrayList<Card> hand,
						ArrayList<Card> board,
						ArrayList<Card> enemyBoard)
	{
	_action = lastAction;
	_active = active;
	_turn = turn;
	_actionPoints = actionPoints;
	_enemyActionPoints = enemyActionPoints;
	_enemyHandSize = enemyHandSize;
	_deckSize = deckSize;
	_hand = hand;
	_board = board;
	_enemyBoard = enemyBoard;
	}
	
	public boolean isActive()
	{
		return _active;
	}
	
	public int getTurn()
	{
		return _turn;
	}
	
	public int getActionPoints()
	{
		return _actionPoints;
	}
	
	public int getEnemyActionPoints()
	{
		return _enemyActionPoints;
	}
	
	public int getEnemyHandSize()
	{
		return _enemyHandSize;
	}
	
	public int getDeckSize()
	{
		return _deckSize;
	}
	
	public ArrayList<Card> getHand()
	{
		return _hand;
	}
	
	public ArrayList<Card> getBoard()
	{
		return _board;
	}
	
	public ArrayList<Card> getEnemyBoard()
	{
		return _enemyBoard;
	}
}
