package adamantTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import adamantServer.*;
import adamantShared.Card;
import adamantShared.UpdateMessage;


public class AdamantServerUnitTests
{

	@Test
	public void testBoard()
	{
		//Creating objects to perform tests
		Board board = new Board();
		Card card = new Card();
		
		//Board should be initially empty
		assertTrue(board.isEmpty());
		
		//Testing appendCard(Card) & size() methods
		board.appendCard(card);
		assertEquals(board.size(), 1);
		
		//Testing contains(Card) & removeCard(Card) methods
		assertTrue(board.contains(card));
		board.removeCard(card);
		
		//Board should be empty
		assertEquals(board.size(), 0);
		assertTrue(board.isEmpty());
	}
	
	@Test
	public void testDeck()
	{
		//Creating objects to perform tests
		Deck deck = new Deck();
		Card[] cards = new Card[10];

		for(int i = 0; i < cards.length; ++i)
		{
			cards[i] = new Card();
			cards[i].setName("card" + i);
		}
			
		//Deck should be initially empty
		assertTrue(deck.isEmpty());
		
		//Testing appendCard(Card) & size() methods
		deck.appendCard(cards[0]);
		assertEquals(deck.size(), 1);
		
		//Testing contains(Card) & removeCard(Card) methods
		assertTrue(deck.contains(cards[0]));
		assertFalse(deck.contains(cards[1]));
		deck.removeCard(cards[0]);
		
		//Deck should be empty
		assertEquals(deck.size(), 0);
		assertTrue(deck.isEmpty());
		
		//Adding cards to the deck for further tests
		for(int i = 0; i < cards.length; ++i)
			deck.appendCard(cards[i]);
		
		//Testing removeCard(int) & size() methods
		assertEquals(cards[cards.length - 1], deck.pickCard());
		assertEquals(deck.size(), 9);
		deck.removeCard(2);
		assertEquals(deck.size(), 8);
		
		//Testing constructor and getters
		deck = new Deck(1, "deck", "it is a deck");
		assertEquals(deck.getId(), 1);
		assertEquals(deck.getName(), "deck");
		assertEquals(deck.getDescription(), "it is a deck");
	}
	
	@Test
	public void testHand()
	{
		//Creating objects to perform tests
		Hand hand = new Hand();
		Card[] cards = new Card[10];

		for(int i = 0; i < cards.length; ++i)
		{
			cards[i] = new Card();
			cards[i].setName("card" + i);
		}
			
		//Hand should be initially empty
		assertTrue(hand.isEmpty());
		
		//Testing appendCard(Card) & size() methods
		hand.appendCard(cards[0]);
		assertEquals(hand.size(), 1);
		
		//Testing contains(Card) & removeCard(Card) methods
		assertTrue(hand.contains(cards[0]));
		assertFalse(hand.contains(cards[1]));
		hand.removeCard(cards[0]);
		
		//Hand should be empty
		assertEquals(hand.size(), 0);
		assertTrue(hand.isEmpty());
		
		//Adding cards to the hand for further tests
		for(int i = 0; i < cards.length; ++i)
			hand.appendCard(cards[i]);
		
		//Testing removeCard(int) & size() methods
		assertEquals(hand.size(), 10);
		hand.removeCard(cards.length - 1);
		assertEquals(hand.size(), 9);
		hand.removeCard(2);
		assertEquals(hand.size(), 8);
	}
	
	@Test
	public void testGameState()
	{
		//Creating objects to perform tests
		GameState gameState = new GameState();
		Deck[] decks = new Deck[2];
		Hand[] hands = new Hand[2];
		for(int i = 0; i < 2; ++i)
		{
			decks[i] = new Deck(i, "deck" + i, "this is deck" + i);
			for(int j = 0; j < 10; ++j)
				decks[i].appendCard(new Card());
			hands[i] = new Hand();
		}
		
		gameState.setDecks(decks);
		gameState.setHands(hands);
		
		//Testing initial variables
		assertFalse(gameState.getGameOver());
		assertEquals(gameState.getWinnerId(), -1);
		assertEquals(gameState.getTurn(), 1);
		assertTrue(gameState.isActive(0));
		assertFalse(gameState.isActive(1));
		assertEquals(gameState.getActionPoints()[0], gameState.getActionPoints()[1]);
		
		//Testing the newTurn() method
		gameState.nextTurn();
		assertEquals(gameState.getTurn(), 2);
		
		//Testing the active player switch
		assertFalse(gameState.isActive(0));
		assertTrue(gameState.isActive(1));
		
		//The second player should have more action points
		assertTrue(gameState.getActionPoints()[0] < gameState.getActionPoints()[1]);
		
		//Testing the player name getter and setter
		gameState.setPlayerName(0, "player0");
		assertEquals(gameState.getPlayerNames()[0], "player0");
		
		//Testing pickCard(int) method
		int handOldSize = gameState.getHands()[0].size();
		gameState.pickCard(0);
		assertEquals(gameState.getHands()[0].size(), handOldSize + 1);
		
		//Testing playCard(int) method
		int boardOldSize = gameState.getBoards()[0].size();
		handOldSize = gameState.getHands()[0].size();
		gameState.playCard(0, 0);
		assertEquals(gameState.getHands()[0].size(), handOldSize - 1);
		assertEquals(gameState.getBoards()[0].size(), boardOldSize + 1);
	}
	
	@Test
	public void testUpdateMessage()
	{
		//Creating objects to perform tests
		Hand hand = new Hand();
		Board board = new Board(), enemyBoard = new Board();
		for(int j = 0; j < 10; ++j)
		{
			hand.appendCard(new Card());
			board.appendCard(new Card());
			enemyBoard.appendCard(new Card());
		}
		
		UpdateMessage message = new UpdateMessage(true, 1, 5, 7, 3, 10, (ArrayList<Card>)hand.getCards(), (ArrayList<Card>)board.getCards(), (ArrayList<Card>)enemyBoard.getCards());
		
		//Testing getters
		assertTrue(message.isActive());
		assertEquals(message.getTurn(), 1);
		assertEquals(message.getActionPoints(), 5);
		assertEquals(message.getEnemyActionPoints(), 7);
		assertEquals(message.getEnemyHandSize(), 3);
		assertEquals(message.getDeckSize(), 10);
		assertEquals(message.getHand().size(), ((ArrayList<Card>)hand.getCards()).size());
		assertEquals(message.getBoard().size(), ((ArrayList<Card>)board.getCards()).size());
		assertEquals(message.getEnemyBoard().size(), ((ArrayList<Card>)enemyBoard.getCards()).size());
	}

}
